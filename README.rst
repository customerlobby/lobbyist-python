===============================
Lobbyist Python
===============================

.. image:: https://img.shields.io/travis/oafridi/lobbyist_python.svg

:target: https://travis-ci.org/oafridi/lobbyist_python

.. image:: https://img.shields.io/pypi/v/lobbyist_python.svg
:target: https://pypi.python.org/pypi/lobbyist_python


Lobbyist for python to communicate with the API project

* Free software: ISC license
* Documentation: https://lobbyist_python.readthedocs.org.

Features
--------

* TODO

How to use
--------

To get scotty info for company with id 10000

>>> import lobbyist_python
>>> lobbyist_python.company.scotty_info(10000)

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-pypackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
