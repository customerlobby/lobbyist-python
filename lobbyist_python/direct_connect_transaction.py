from lobbyist_python.base import Base


class DirectConnectTransaction(Base):
    TRANSACTIONS_PATH = Base.build_url('direct-connect-transactions')

    @staticmethod
    def create(params=None):
        """Create a transaction.

        :param params: params required to create a transaction
        :return: json of newly created transaction
        """
        return DirectConnectTransaction.post(params, DirectConnectTransaction.TRANSACTIONS_PATH)
