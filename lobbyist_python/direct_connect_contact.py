from lobbyist_python.base import Base


class DirectConnectContact(Base):
    CONTACTS_PATH = Base.build_url('direct-connect-contacts')

    @staticmethod
    def create(params=None):
        """Create a contact.

        :param params: params required to create a contact
        :return: json of newly created contact
        """
        return DirectConnectContact.post(params, DirectConnectContact.CONTACTS_PATH)
