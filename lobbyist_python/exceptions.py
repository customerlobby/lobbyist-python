class BadGateway(Exception):
    HTTP_STATUS_CODE = 502


class BadRequest(Exception):
    HTTP_STATUS_CODE = 400


class DecodeError(Exception):
    HTTP_STATUS_CODE = 000


class Forbidden(Exception):
    HTTP_STATUS_CODE = 403


class GatewayTimeout(Exception):
    HTTP_STATUS_CODE = 504


class InternalServerError(Exception):
    HTTP_STATUS_CODE = 500


class NotAcceptable(Exception):
    HTTP_STATUS_CODE = 406


class NotFound(Exception):
    HTTP_STATUS_CODE = 404


class PreconditionFailed(Exception):
    HTTP_STATUS_CODE = 412


class ServiceUnavailable(Exception):
    HTTP_STATUS_CODE = 503


class Unauthorized(Exception):
    HTTP_STATUS_CODE = 401


class UnprocessableEntity(Exception):
    HTTP_STATUS_CODE = 422
