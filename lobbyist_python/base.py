import requests
from simplejson import JSONDecodeError

from lobbyist_python.exceptions import *
from .lobbyist import Lobbyist


class Base:
    @staticmethod
    def build_url(resource):
        return '/v2/' + resource

    @staticmethod
    def post(params, path):
        url = Lobbyist.api_base + path
        response = requests.post(url, headers=Lobbyist.headers(), json=params)
        return Base.handle_response(response)

    @staticmethod
    def get(params, path):
        url = Lobbyist.api_base + path
        response = requests.get(url, headers=Lobbyist.headers(), json=params)
        return Base.handle_response(response)

    @staticmethod
    def handle_response(response):
        switcher = {
            400: BadRequest,
            401: Unauthorized,
            403: Forbidden,
            412: PreconditionFailed,
            500: InternalServerError,
        }
        try:
            if switcher.get(response.status_code):
                raise switcher[response.status_code](response.json())
            else:
                return response.json()
        except JSONDecodeError:
            raise DecodeError("Unparsable Response: %s" % response.text)
