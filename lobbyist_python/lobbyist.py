# -*- coding: utf-8 -*-
import os


class Lobbyist:
    """This class holds the connection string attributes such as key and base url.
    """

    os.environ['LOBBYIST_API_KEY'] = 'Jz43pwlU1rb4ewhgltWO'  # Just for testing
    os.environ['LOBBYIST_API_BASE'] = 'http://localhost:3000'  # Just for testing

    api_base = os.environ['LOBBYIST_API_BASE']
    api_key = os.environ['LOBBYIST_API_KEY']

    @staticmethod
    def headers():
        return {
            'Accept': 'application/json',
            'Authorization': "Token token=" + "\"" + Lobbyist.api_key + "\""
        }
