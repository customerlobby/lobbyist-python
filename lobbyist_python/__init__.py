# -*- coding: utf-8 -*-
"""
Lobbyist Python

This is a library written in python for communicating with the customer lobby API. Basic usage:

>>> import lobbyist_python
>>> lobbyist_python.scotty_info
blah blah blah

"""
__author__ = 'Osman Afridi'
__email__ = 'oafridi@customerlobby.com'
__version__ = '0.1.1'

from . import company, direct_connect_contact, direct_connect_transaction
