from lobbyist_python.base import Base


class Company(Base):
    COMPANY_PATH = Base.build_url('companies/')

    @staticmethod
    def scotty_info(id_):
        """Get scotty info.

        :param id_: the company id.
        :return: json of scotty info for the company.
        """
        return Company.get(id_, Company.path(id_))

    @staticmethod
    def path(id_):
        return Company.COMPANY_PATH + str(id_) + '/scotty_info'
