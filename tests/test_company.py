import unittest
import responses

from lobbyist_python import company
from lobbyist_python.lobbyist import Lobbyist


class TestCompany(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.test_id = 10000

    @classmethod
    def tearDownClass(cls):
        cls.test_id = None

    @responses.activate
    def test_scotty_info_fail(self):
        url = Lobbyist.api_base + '/v2/companies/' + str(self.test_id) + '/scotty_info'

        responses.add(responses.GET, url,
                      body='{"error": "not found"}', status=404,
                      content_type='application/json')

        self.assertEqual(company.Company.scotty_info(self.test_id), {"error": "not found"})

    @responses.activate
    def test_scotty_info_succeed(self):
        url = Lobbyist.api_base + '/v2/companies/' + str(self.test_id) + '/scotty_info'

        responses.add(responses.GET, url, json={
            'company': {'address2': '', 'sales_user_name': 'Wayne Byers', 'company_name': 'DrumART.com', 'id': 10000,
                        'customer_call_notes': None,
                        'handwritten_form_url': 'https://s3.amazonaws.com/customerlobby-docs-development/handwritten-review-forms/10000/Handwritten_Review.pdf',
                        'longitude': '-74.004676', 'timezone': None, 'account_manager_id': None, 'status': 'terminated',
                        'latitude': '42.659958', 'created_at': '2012-01-27T12:14:32Z', 'phone': '8773786278',
                        'account_level_id': 11,
                        'review_page_url': 'http://dev.customerlobby.com/reviews/10000/drumartcom/',
                        'activation_code': '2405a461204c5f1b2', 'system_of_record': None,
                        'qualifies_for_free_month': True, 'phone_ext': None, 'enterprise_id': None, 'city': 'Altamont',
                        'website': 'http://www.drumart.com/', 'average_score': 5,
                        'custom_invitation_pdf_url': 'https://s3.amazonaws.com/customerlobby-docs-development/custom-invitation-forms/10000/Custom_Invitation.pdf',
                        'customer_downloads_url': 'https://s3.amazonaws.com/customerlobby-docs-development/customer-downloads/c2c74d15c17f5eb9d7f5716e3126e4a5.csv',
                        'formatted_address': '41 Indian Ladder Drive, Altamont, NY 12009, USA',
                        'admin_notes': '01/30/12 kb: Left vm for Jim. Funny thing: He just called in for a support request about putting the button on his site. I then notice the account has been cancelled. \nRatepoint',
                        'updated_at': '2015-08-30T23:36:07Z', 'current_balance': '0.0',
                        'address1': '41 Indian Ladder Drive', 'direct_connect_status_date': None,
                        'company_description': '', 'zip': '12009',
                        'custom_invitation_url': 'http://dev.customerlobby.com/reviews/10000/drumartcom/custom-invitation',
                        'kiosk_page_url': 'http://dev.customerlobby.com/reviews/10000/drumartcom/mobile-kiosk',
                        'promo_id': 133, 'freetrial_start_date': '2012-01-27T12:14:32Z',
                        'abbreviated_name': 'drumartcom', 'partner_id': None, 'reviews_count': 704,
                        'account_class': 'member', 'direct_connect_status': 'terminated', 'is_active': True,
                        'trial_source': 'outbound', 'termination_date': '2012-01-30T15:30:31Z', 'company_id': 10000,
                        'country': 'US', 'account_terminated': True, 'partner_account_id': None, 'state': 'NY'},
            'urls': {'company_url': 'http://localhost:3000/v2/companies/10000',
                     'scotty_download_url': 'http://customerlobby-assemblies-production.s3.amazonaws.com/CustomerLobbyDirectConnectSetup.exe'},
            'workflow_system': None, 'scotty_setting': None}, status=200)

        self.assertEqual(company.Company.scotty_info(self.test_id), {
            'company': {'address2': '', 'sales_user_name': 'Wayne Byers', 'company_name': 'DrumART.com', 'id': 10000,
                        'customer_call_notes': None,
                        'handwritten_form_url': 'https://s3.amazonaws.com/customerlobby-docs-development/handwritten-review-forms/10000/Handwritten_Review.pdf',
                        'longitude': '-74.004676', 'timezone': None, 'account_manager_id': None, 'status': 'terminated',
                        'latitude': '42.659958', 'created_at': '2012-01-27T12:14:32Z', 'phone': '8773786278',
                        'account_level_id': 11,
                        'review_page_url': 'http://dev.customerlobby.com/reviews/10000/drumartcom/',
                        'activation_code': '2405a461204c5f1b2', 'system_of_record': None,
                        'qualifies_for_free_month': True, 'phone_ext': None, 'enterprise_id': None, 'city': 'Altamont',
                        'website': 'http://www.drumart.com/', 'average_score': 5,
                        'custom_invitation_pdf_url': 'https://s3.amazonaws.com/customerlobby-docs-development/custom-invitation-forms/10000/Custom_Invitation.pdf',
                        'customer_downloads_url': 'https://s3.amazonaws.com/customerlobby-docs-development/customer-downloads/c2c74d15c17f5eb9d7f5716e3126e4a5.csv',
                        'formatted_address': '41 Indian Ladder Drive, Altamont, NY 12009, USA',
                        'admin_notes': '01/30/12 kb: Left vm for Jim. Funny thing: He just called in for a support request about putting the button on his site. I then notice the account has been cancelled. \nRatepoint',
                        'updated_at': '2015-08-30T23:36:07Z', 'current_balance': '0.0',
                        'address1': '41 Indian Ladder Drive', 'direct_connect_status_date': None,
                        'company_description': '', 'zip': '12009',
                        'custom_invitation_url': 'http://dev.customerlobby.com/reviews/10000/drumartcom/custom-invitation',
                        'kiosk_page_url': 'http://dev.customerlobby.com/reviews/10000/drumartcom/mobile-kiosk',
                        'promo_id': 133, 'freetrial_start_date': '2012-01-27T12:14:32Z',
                        'abbreviated_name': 'drumartcom', 'partner_id': None, 'reviews_count': 704,
                        'account_class': 'member', 'direct_connect_status': 'terminated', 'is_active': True,
                        'trial_source': 'outbound', 'termination_date': '2012-01-30T15:30:31Z', 'company_id': 10000,
                        'country': 'US', 'account_terminated': True, 'partner_account_id': None, 'state': 'NY'},
            'urls': {'company_url': 'http://localhost:3000/v2/companies/10000',
                     'scotty_download_url': 'http://customerlobby-assemblies-production.s3.amazonaws.com/CustomerLobbyDirectConnectSetup.exe'},
            'workflow_system': None, 'scotty_setting': None})


if __name__ == '__main__':
    unittest.main()
