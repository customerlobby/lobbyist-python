import unittest

from requests import Response

from lobbyist_python.base import Base
from lobbyist_python.exceptions import *


class TestBase(unittest.TestCase):
    def test_handle_response(self):
        response = Response()
        response.status_code = 412
        response._content = b'{"errors":["Required Parameters: company_id, cleaned_contact are missing."]}'
        self.assertRaises(PreconditionFailed, Base.handle_response, response)


if __name__ == '__main__':
    unittest.main()
