import unittest
from unittest.mock import patch
from lobbyist_python.direct_connect_contact import DirectConnectContact


class TestContact(unittest.TestCase):
    @patch.object(DirectConnectContact, 'post')
    def test_create_valid(self, post):
        DirectConnectContact.create({})
        post.assert_called_with({}, DirectConnectContact.CONTACTS_PATH)


if __name__ == '__main__':
    unittest.main()
