import unittest
from unittest.mock import patch
from lobbyist_python.direct_connect_transaction import DirectConnectTransaction


class TestContact(unittest.TestCase):
    @patch.object(DirectConnectTransaction, 'post')
    def test_create_valid(self, post):
        DirectConnectTransaction.create({})
        post.assert_called_with({}, DirectConnectTransaction.TRANSACTIONS_PATH)


if __name__ == '__main__':
    unittest.main()
