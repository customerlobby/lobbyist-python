import unittest

from lobbyist_python.lobbyist import Lobbyist


class TestLobbyistPython(unittest.TestCase):
    def test_headers(cls):
        TEST_KEY = "test"
        Lobbyist.api_key = TEST_KEY

        header = {
            'Accept': 'application/json',
            'Authorization': "Token token=" + "\"" + TEST_KEY + "\""
        }

        cls.assertEqual(Lobbyist.headers(), header)


if __name__ == '__main__':
    import sys

    sys.exit(unittest.main())
