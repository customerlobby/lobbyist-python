============
Installation
============

At the command line::

    $ easy_install lobbyist_python

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv lobbyist_python
    $ pip install lobbyist_python
